import csv
from .models import Matches, Deliveries


def delete_deliveries_database():
    try:
        Matches.objects.all().delete()
    except Exception as e:
        print("error = ", e)
        return False
    return True


def is_matches_loaded():
    filename = 'ipldataset/ipldata/matches.csv'
    matches_load = True
    try:
        with open(filename) as file:
            matches_reader = csv.DictReader(file)
            for match in matches_reader:
                _, created = Matches.objects.create(
                    id=match['id'],
                    season=match['season'],
                    city=match['city'],
                    date=match['date'],
                    team1=match['team1'],
                    team2=match['team2'],
                    toss_winner=match['toss_winner'],
                    toss_decision=match['toss_decision'],
                    result=match['result'],
                    dl_applied=match['dl_applied'],
                    winner=match['winner'],
                    win_by_runs=match['win_by_runs'],
                    win_by_wickets=match['win_by_wickets'],
                    player_of_match=match['player_of_match'],
                    venue=match['venue'],
                    umpire1=match['umpire1'],
                    umpire2=match['umpire2'],
                    umpire3=match['umpire3'])
    except Exception:
        matches_load = False

    return matches_load


def is_delivery_loaded():
    filename = 'ipldataset/ipldata/deliveries.csv'
    delivery_load = True
    try:
        with open(filename) as file:
            delivery_reader = csv.DictReader(file)
            delivery_obj = []
            h = 1
            for delivery in delivery_reader:
                match_id = Matches.objects.get(id=delivery['match_id'])
                deliveryobj = Deliveries(
                    match_id=match_id,
                    inning=delivery['inning'],
                    batting_team=delivery['batting_team'],
                    bowling_team=delivery['bowling_team'],
                    over=delivery['over'],
                    ball=delivery['ball'],
                    batsman=delivery['batsman'],
                    non_striker=delivery['non_striker'],
                    bowler=delivery['bowler'],
                    is_super_over=delivery['is_super_over'],
                    wide_runs=delivery['wide_runs'],
                    bye_runs=delivery['bye_runs'],
                    legbye_runs=delivery['legbye_runs'],
                    noball_runs=delivery['noball_runs'],
                    penalty_runs=delivery['penalty_runs'],
                    batsman_runs=delivery['batsman_runs'],
                    extra_runs=delivery['extra_runs'],
                    total_runs=delivery['total_runs'],
                    player_dismissed=delivery['player_dismissed'],
                    dismissal_kind=delivery['dismissal_kind'],
                    fielder=delivery['fielder'])
                delivery_obj.append(deliveryobj)
                print(h)
                h += 1
            print(len(delivery_obj))
            Deliveries.objects.bulk_create(delivery_obj)
    except Exception as e:
        print("error =", e)
        delivery_load = False

    return delivery_load
