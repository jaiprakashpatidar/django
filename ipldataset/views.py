from django.shortcuts import render
from django.db.models import Count, Sum, F
from .load_data import (is_matches_loaded, is_delivery_loaded,
                        delete_deliveries_database)
from .load_data import delete_deliveries_database
from .models import Matches, Deliveries
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.core.cache import cache
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


def get_dicionary_of_years(years):
    year_wise_win_initialization = {}
    for year in years:
        year_wise_win_initialization[year] = 0
    return year_wise_win_initialization


@api_view(['GET'])
def total_match_played_per_season(request):
    if 'total_match_played' in cache:
        match_played_per_year = cache.get('total_match_played')
        print("cache = ", match_played_per_year)
        return render(request, 'ipldataset/graph1.html',
                      {'total_match_played': match_played_per_year})
    else:
        total_match_played_per_season = Matches.objects.values('season').\
            annotate(Count('season'))
        match_played_per_year = []
        for season in total_match_played_per_season:
            match_played_per_year.append({'name': str(season['season']),
                                          'data': [season['season__count']]})
        # print(match_played_per_year)
        cache.set('total_match_played',
                  match_played_per_year, timeout=CACHE_TTL)

        return render(request, 'ipldataset/graph1.html',
                      {'total_match_played': match_played_per_year})


@api_view(['GET'])
def teams_win_per_year(request):
    if 'teams_win' in cache:
        teams_win_per_year = cache.get('teams_win')
        print("Cache =  ", teams_win_per_year)
        return render(request, 'ipldataset/graph2.html',
                      {'teams_win': teams_win_per_year})
    else:
        teams_win_per_year = Matches.objects.values_list('season', 'winner').\
            annotate(Count('winner')).\
            order_by('season')
        years = [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017]
        teams_win = {}
        for team in teams_win_per_year:
            print(team[1], team[0])
            team_name = team[1]
            if team_name == 'Rising Pune Supergiant':
                team_name = 'Rising Pune Supergiants'
            if team_name not in teams_win:
                teams_win[team_name] = get_dicionary_of_years(years)
                teams_win[team_name][team[0]] = int(team[2])
            else:
                if team[0] not in teams_win[team_name]:
                    teams_win[team_name][team[0]] = 0
                teams_win[team_name][team[0]] += int(team[2])
        print(teams_win)
        teams_win_per_year = []
        for team in teams_win:
            team_json = {'name': team,
                         'data': [no_of_wins[1] for no_of_wins in teams_win[team].items()]}
            print(team)
            print(team_json['data'])
            teams_win_per_year.append(team_json)
        print(teams_win_per_year)
        cache.set('teams_win', teams_win_per_year, timeout=CACHE_TTL)
        return render(request, 'ipldataset/graph2.html',
                      {'teams_win': teams_win_per_year})


@api_view(['GET'])
def extra_runs_per_team_of_2016(request):
    if 'extra_runs' in cache:
        teams_extra_runs = cache.get('extra_runs')
        print(" cache = ", teams_extra_runs)
        return render(request, 'ipldataset/graph3.html', {'extra_runs': teams_extra_runs})
    else:
        # ids = Matches.objects.filter(season=2016).values_list('id', flat=True)
        # print("id = ", list(ids))
        extra_runs_per_team_of_2016 = Deliveries.objects.filter(
            match_id__in=Matches.objects\
            .filter(season=2016).select_related()\
            .values_list('id', flat=True))\
        .values_list('bowling_team')\
        .annotate(data=Sum('extra_runs'))
        print("runss=", list(extra_runs_per_team_of_2016))
        teams_extra_runs = []
        for team in extra_runs_per_team_of_2016:
            teams_extra_runs.append({'name': team[0],
                                     'data': [{'y': team[1], 'drilldown':team[0]}]})
        print()
        print("ex=", teams_extra_runs)
        cache.set('extra_runs', teams_extra_runs, timeout=CACHE_TTL)
        return render(request, 'ipldataset/graph3.html', {'extra_runs': teams_extra_runs})


@api_view(['GET'])
def top_bowlers_economy(request):
    if 'bowlers_economic' in cache:
        top_bowlers_economic = cache.get('bowlers_economic')
        print("cache= ", top_bowlers_economic)
        return render(request, 'ipldataset/graph4.html',
                      {'bowlers_economic': top_bowlers_economic})
    else:
        # ids = 
        bowlers_data = Deliveries.objects\
            .filter(match_id__in=Matches.objects.filter(season=2015)\
            .values_list('id', flat=True)).\
            values_list('bowler').annotate(Count('ball')).\
            annotate(runs=Sum('total_runs')).\
            annotate(legbye=Sum('legbye_runs')).\
            annotate(Sum('bye_runs')).\
            order_by('bowler')
        print(bowlers_data)
        # bowlers_economic = []
        bowlers_economic = {}
        for bowler in bowlers_data:
            bowler_name = bowler[0]
            over = int(bowler[1]/6)
            runs = bowler[2]-bowler[3]-bowler[4]
            economy = round(runs/over, 2)
            bowlers_economic[bowler_name] = economy
        print(bowlers_economic)
        print("\n\n\n")
        bowlers_economic = sorted(bowlers_economic.items(),
                                  key=lambda economy: economy[1])[0:5]
        print(bowlers_economic)
        top_bowlers_economic = [{'name': bowler[0], 'data':[
            bowler[1]]}for bowler in bowlers_economic]
        print(top_bowlers_economic)
        cache.set('bowlers_economic', top_bowlers_economic, timeout=CACHE_TTL)
        return render(request, 'ipldataset/graph4.html',
                      {'bowlers_economic': top_bowlers_economic})


@api_view(['GET'])
def top_batsman(request):
    if 'top_batsman' in cache:
        top_batsman = cache.get('top_batsman')
        print("cache = ", top_batsman)
        return render(request, 'ipldataset/graph5.html',
                      {'top_batsman': top_batsman})
    else:
        batsman_details = Deliveries.objects.annotate(name=F('batsman')).values(
            'name').annotate(data=Sum('batsman_runs')).order_by('data', 'name')
        # temp = json.dumps(batsman_details)
        batsman_details = reversed(
            batsman_details[len(list(batsman_details))-5:len(list(batsman_details))])
        print(batsman_details)

        # print(temp)
        top_batsman = []
        for batsman in batsman_details:
            batsman_name = batsman['name']
            batsman_runs = batsman['data']
            batsman_json = {
                'name': batsman_name,
                'data': [batsman_runs]
            }
            top_batsman.append(batsman_json)
        cache.set('top_batsman', top_batsman, timeout=CACHE_TTL)
        return render(request, 'ipldataset/graph5.html',
                      {'top_batsman': top_batsman})
