from django.apps import AppConfig


class IpldatasetConfig(AppConfig):
    name = 'ipldataset'
