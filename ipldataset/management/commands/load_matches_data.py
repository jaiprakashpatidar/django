from django.core.management.base import BaseCommand, CommandError
from ipldataset.models import Matches
from django.utils import timezone
from django.db import transaction
import csv


class Command(BaseCommand):
    help = 'Displays current time'

    def handle(self, *args, **kwargs):
        filename = 'ipldataset/ipldata/matches.csv'
        message = 'Data Loaded Succesfully'
        
        try:
            with transaction.atomic():
                with open(filename) as file:
                    matches_reader = csv.DictReader(file)
                    match_obj = []
                    for match in matches_reader:
                        obj = Matches(
                            id=match['id'],
                            season=match['season'],
                            city=match['city'],
                            date=match['date'],
                            team1=match['team1'],
                            team2=match['team2'],
                            toss_winner=match['toss_winner'],
                            toss_decision=match['toss_decision'],
                            result=match['result'],
                            dl_applied=match['dl_applied'],
                            winner=match['winner'],
                            win_by_runs=match['win_by_runs'],
                            win_by_wickets=match['win_by_wickets'],
                            player_of_match=match['player_of_match'],
                            venue=match['venue'],
                            umpire1=match['umpire1'],
                            umpire2=match['umpire2'],
                            umpire3=match['umpire3'])
                        match_obj.append(obj)
                    print("hello")
                Matches.objects.bulk_create(match_obj)
        except Exception as e:
            print(e)
            message = 'Failed to load '
        self.stdout.write(message)
