from django.core.management.base import BaseCommand, CommandError
from ipldataset.models import Deliveries, Matches
from django.utils import timezone
from django.db import transaction
import csv


class Command(BaseCommand):
    help = 'Displays current time'

    def handle(self, *args, **kwargs):
        filename = 'ipldataset/ipldata/deliveries.csv'
        message = 'Delivery data loaded Successfully'
        try:
            with transaction.atomic():
                with open(filename) as file:
                    delivery_reader = csv.DictReader(file)
                    delivery_obj = []
                    h = 1
                    for delivery in delivery_reader:
                        match_id = Matches.objects.get(id=delivery['match_id'])
                        deliveryobj = Deliveries(
                            match_id=match_id,
                            inning=delivery['inning'],
                            batting_team=delivery['batting_team'],
                            bowling_team=delivery['bowling_team'],
                            over=delivery['over'],
                            ball=delivery['ball'],
                            batsman=delivery['batsman'],
                            non_striker=delivery['non_striker'],
                            bowler=delivery['bowler'],
                            is_super_over=delivery['is_super_over'],
                            wide_runs=delivery['wide_runs'],
                            bye_runs=delivery['bye_runs'],
                            legbye_runs=delivery['legbye_runs'],
                            noball_runs=delivery['noball_runs'],
                            penalty_runs=delivery['penalty_runs'],
                            batsman_runs=delivery['batsman_runs'],
                            extra_runs=delivery['extra_runs'],
                            total_runs=delivery['total_runs'],
                            player_dismissed=delivery['player_dismissed'],
                            dismissal_kind=delivery['dismissal_kind'],
                            fielder=delivery['fielder'])
                        delivery_obj.append(deliveryobj)
                        print(f'{h} Rows Parsed')
                        h += 1
                    print(len(delivery_obj))
                    Deliveries.objects.bulk_create(delivery_obj)
        except Exception as e:
            print("error =", e)
            message = 'Failed to Load Delivery Data'
        self.stdout.write(message)
