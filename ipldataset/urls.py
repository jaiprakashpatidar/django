from django.urls import path
from . import views

urlpatterns = [
    path('', views.total_match_played_per_season, name="home"),
    path('graph1.html', views.total_match_played_per_season, name="chart-1"),
    path('graph2.html', views.teams_win_per_year, name="chart-2"),
    path('graph3.html', views.extra_runs_per_team_of_2016, name="chart-3"),
    path('graph4.html', views.top_bowlers_economy, name="chart-4"),
    path('graph5.html', views.top_batsman, name="chart-5"),
    # path('about/', views.about, name="blog-about"),

]
